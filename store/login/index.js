export const state = () => ({
  errors: []
})

export const actions = {
  async submitLogin ({ state, commit }, { email, password }) {
    return await this.$auth.loginWith('local', { data: { email, password } })
      .then(() => this.$router.push('/users'))
  },
  async logout () {
    return await this.$auth.logout()
      .then(() => this.$router.push('/login'))
  }
}

export const getters = {
  state: state => state,
  errors: state => state.errors
}
