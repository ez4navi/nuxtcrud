export const state = () => ({
  loading: false
})

export const actions = {
  showLoader ({ commit }) {
    commit('SHOW_LOADER')
  },
  hideLoader ({ commit }) {
    commit('HIDE_LOADER')
  }
}

export const mutations = {
  SHOW_LOADER: (state) => {
    state.loading = true
  },
  HIDE_LOADER: (state) => {
    state.loading = false
  }
}

export const getters = {
  state: state => state,
  loading: state => state.loading
}
