export const state = () => ({
  items: [],
  currentPage: 1,
  totalCount: 0,
  openModal: false,
  creatingUserStatus: false,
  tableLoading: false
})

export const actions = {
  async createUser ({ dispatch, commit, state }, form) {
    const { data: userData } = await this.$axios.post('/users', form)

    const response = await this.$axios.get(`/users?_page=${state.currentPage}`)
    const { headers, data } = response

    commit('SET_TOTAL_COUNT', Number(headers['x-total-count']))
    commit('SET_USERS', data)

    return userData
  },
  async patch ({ commit, state }, { id, email }) {
    const { data } = await this.$axios.patch(`/users/${id}`, {
      email
    })
    commit('PATCH_USER', data)
    return data
  },
  async deleteUser ({ commit, state }, id) {
    await this.$axios.delete(`/users/${id}`)
    const response = await this.$axios.get(`/users?_page=${state.currentPage}`)

    const { headers, data } = response

    commit('SET_TOTAL_COUNT', Number(headers['x-total-count']))
    commit('SET_USERS', data)

    return data
  },
  async fetchUsers ({ commit, state }) {
    const response = await this.$axios.get(`/users?_page=${state.currentPage}`)
    const { headers, data } = response

    commit('SET_TOTAL_COUNT', Number(headers['x-total-count']))
    commit('SET_USERS', data)

    return data
  },
  changeUsersItems ({ commit }, data) {
    commit('SET_USERS', data)
  },
  changeCurrentPage ({ commit }, data) {
    commit('SET_CURRENT_PAGE', data)
  },
  changeOpenState ({ commit }) {
    commit('CHANGE_OPEN_STATE')
  },
  startLoading ({ commit }) {
    commit('START_LOADING')
  },
  stopLoading ({ commit }) {
    commit('STOP_LOADING')
  },
  startTableLoading ({ commit }) {
    commit('START_TABLE_LOADING')
  },
  stopTableLoading ({ commit }) {
    commit('STOP_TABLE_LOADING')
  }
}

export const mutations = {
  SET_USERS: (state, data) => {
    state.items = data
  },
  PATCH_USER: (state, data) => {
    state.items = state.items.map(item => item.id === data.id ? data : item)
  },
  SET_TOTAL_COUNT: (state, data) => {
    state.totalCount = data
  },
  SET_CURRENT_PAGE: (state, data) => {
    state.currentPage = data
  },
  CHANGE_OPEN_STATE: (state) => {
    state.openModal = !state.openModal
  },
  START_LOADING: (state) => {
    state.creatingUserStatus = true
  },
  STOP_LOADING: (state) => {
    state.creatingUserStatus = false
  },
  START_TABLE_LOADING: (state) => {
    state.tableLoading = true
  },
  STOP_TABLE_LOADING: (state) => {
    state.tableLoading = false
  }
}

export const getters = {
  state: state => state,
  items: state => state.items,
  totalCount: state => state.totalCount,
  currentPage: state => state.currentPage,
  openModal: state => state.openModal,
  creatingUserStatus: state => state.creatingUserStatus,
  tableLoading: state => state.tableLoading
}
